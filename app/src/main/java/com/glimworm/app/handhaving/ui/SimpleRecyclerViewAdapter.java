package com.glimworm.app.handhaving.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by lars on 30-03-16.
 */
public class SimpleRecyclerViewAdapter<T, S extends SimpleViewHolder<T>> extends RecyclerView.Adapter<S> {

    public interface OnCreateCallback<S> {
        S onCreateViewHolder(Context context, LayoutInflater inflater, ViewGroup parent);
    }

    private Context context;
    private OnCreateCallback<S> callback;
    private List<T> list;

    public SimpleRecyclerViewAdapter(@NonNull Context context,
                                     @NonNull List<T> list,
                                     @NonNull OnCreateCallback<S> callback) {
        this.context = context;
        this.callback = callback;
        this.list = list;
    }

    @Override
    public S onCreateViewHolder(ViewGroup parent, int viewType) {
        return callback.onCreateViewHolder(context, LayoutInflater.from(context), parent);
    }

    @Override
    public void onBindViewHolder(S holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
