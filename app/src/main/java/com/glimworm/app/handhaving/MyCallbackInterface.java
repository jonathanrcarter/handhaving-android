package com.glimworm.app.handhaving;

/**
 * Created by jc on 20/11/15.
 */
//define callback interface
public interface MyCallbackInterface {

    void onDownloadFinished(String result);
}