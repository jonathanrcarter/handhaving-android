package com.glimworm.app.handhaving.services;

import com.glimworm.app.handhaving.responses.parkingcams.ParkingCam;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by lars on 10-03-16.
 */
public interface LivingLabService {

    @GET("parkingcams")
    Call<List<ParkingCam>> getParkingCams();
}