package com.glimworm.app.handhaving.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.glimworm.app.handhaving.DetailActivity;
import com.glimworm.app.handhaving.R;
import com.glimworm.app.handhaving.helpers.API;
import com.glimworm.app.handhaving.responses.ParkingCamsResponse;
import com.glimworm.app.handhaving.responses.parkingcams.ParkingCam;
import com.glimworm.app.handhaving.services.LivingLabService;
import com.glimworm.app.handhaving.ui.ParkingCamViewHolder;
import com.glimworm.app.handhaving.ui.SimpleRecyclerViewAdapter;
import com.glimworm.app.handhaving.ui.listeners.OnItemClickListener;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lars on 29-03-16.
 */
public class CamsFragment extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener,
        Callback<List<ParkingCam>>,
        OnItemClickListener<ParkingCam> {

    private static final String TAG = "Camsfragment";
    private LivingLabService service;
    private List<ParkingCam> cams;
    private SimpleRecyclerViewAdapter<ParkingCam, ParkingCamViewHolder> adapter;
    private MqttAndroidClient client;

    @Bind(R.id.recyclerview)
    RecyclerView recyclerView;
    @Bind(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cams, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        service = API.getService();
        cams = new ArrayList<>();
        adapter = new SimpleRecyclerViewAdapter<>(getContext(), cams,
                new SimpleRecyclerViewAdapter.OnCreateCallback<ParkingCamViewHolder>() {
                    @Override
                    public ParkingCamViewHolder onCreateViewHolder(Context context,
                                                                   LayoutInflater inflater,
                                                                   ViewGroup parent) {
                        return new ParkingCamViewHolder(
                                context,
                                inflater.inflate(R.layout.listitem_cam, parent, false),
                                CamsFragment.this
                        );
                    }
                });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        refreshLayout.setOnRefreshListener(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter.setHasStableIds(false);
        recyclerView.setAdapter(adapter);

        service.getParkingCams().enqueue(this);

        Log.i(TAG,"AAAAAA CREATE AAAAAA");
        String clientId = MqttClient.generateClientId();
        client =
                new MqttAndroidClient(this.getActivity().getApplicationContext(), "tcp://dev.ibeaconlivinglab.com:1883",
                        clientId);

        client.setCallback(new MqttCallback(){

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.i(TAG,"T:"+topic);
                Log.i(TAG,"P:"+message.getPayload());
                String[] _topic = topic.split("[/]");

                String value = new String(message.getPayload(), "UTF-8");
                Log.i(TAG,"VP:"+value);

                for (int i=0; i < cams.size(); i++) {

                    if (_topic.length > 0 && _topic[0] != null && _topic[0].equalsIgnoreCase("datalab")) {
                        if (_topic.length > 1 && _topic[1] != null && _topic[1].equalsIgnoreCase(cams.get(i).getLora_appid())) {
                            if (_topic.length > 2 && _topic[2] != null && _topic[2].equalsIgnoreCase(cams.get(i).getLora_devid())) {
                                if (_topic.length > 3 && _topic[3] != null && _topic[3].equalsIgnoreCase("rx")) {
                                    JSONObject reader = new JSONObject(value);
                                    Log.i(TAG, "JP:" + reader.toString(4));
//                                        cams.get(i).setStatus(reader.optInt("status",0));
                                    if (reader.optInt("status",0) == 0) {
                                        cams.get(i).setStatus(ParkingCam.Status.EMPTY);

                                    }
                                    if (reader.optInt("status",0) == 1) {
                                        cams.get(i).setStatus(ParkingCam.Status.CAR);

                                    }
                                    if (reader.optInt("status",0) == 2) {
                                        cams.get(i).setStatus(ParkingCam.Status.TRUCK);

                                    }
                                    adapter.notifyItemChanged(i);
                                }
                            }
                        }
                    }
                }


            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
        try {
            IMqttToken token = client.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.i(TAG, "onSuccess");
                    String topic = "#";
                    int qos = 1;
                    try {
                        IMqttToken subToken = client.subscribe(topic, qos);
                        subToken.setActionCallback(new IMqttActionListener() {
                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                // The message was published
                                Log.i(TAG, asyncActionToken.getTopics().toString());
                                Log.i(TAG, Integer.toString(asyncActionToken.getMessageId()));
                                try {
//                                    Log.d(TAG, asyncActionToken.getResponse().getPayload().toString());
                                } catch (Exception E) {
                                    E.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(IMqttToken asyncActionToken,
                                                  Throwable exception) {
                                // The subscription could not be performed, maybe the user was not
                                // authorized to subscribe on the specified topic e.g. using wildcards

                            }

                        });
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.d(TAG, "onFailure");

                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Call<List<ParkingCam>> call, Response<List<ParkingCam>> response) {
        cams.clear();
        //test
        cams.addAll(response.body());
        adapter.notifyDataSetChanged();
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<List<ParkingCam>> call, Throwable t) {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        service.getParkingCams().enqueue(this);
    }

    @Override
    public void onClick(ParkingCam item, View view, int position) {
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra(DetailActivity.LAT_KEY, item.getLat());
        intent.putExtra(DetailActivity.LON_KEY, item.getLon());
        intent.putExtra(DetailActivity.NAME_KEY, item.getDisplayname());

        startActivity(intent);
    }
}
