package com.glimworm.app.handhaving.responses.parkingcams;

import com.google.gson.internal.bind.DateTypeAdapter;

import java.util.Date;

/**
 * Created by lars on 10-03-16.
 */
public class ParkingCam {



    public enum Status {
        EMPTY, CAR, TRUCK, CAR_VIOLATION, TRUCK_VIOLATION
    }

    private String key;
    private Status status;
    private int front;
    private int bottom;
    private int battery;
    private Date timestamp;
    private double lat = 52.353505;
    private double lon = 4.891220;
    private String lora_appid = "";
    private String lora_devid = "";
    private String displayname = "";

    public String getKey() {
        return key;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    public Status getStatus() {
        return status;
    }

    public int getFront() {
        return front;
    }

    public int getBottom() {
        return bottom;
    }

    public int getBattery() {
        return battery;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getLora_appid() {
        return lora_appid;
    }

    public String getLora_devid() {
        return lora_devid;
    }

    public String getDisplayname() {
        return displayname;
    }

}