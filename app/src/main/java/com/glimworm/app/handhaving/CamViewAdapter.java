package com.glimworm.app.handhaving;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.glimworm.app.handhaving.helpers.data;
import com.squareup.picasso.Picasso;
//import com.squareup.picasso.Picasso;


/**
 * Created by jc on 15/11/15.
 */
public class CamViewAdapter extends BaseAdapter {

//    public ArrayList<HashMap<String,String>> list;
    Activity activity;

//    public ListViewAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
    public CamViewAdapter(Activity activity) {
        super();
        this.activity = activity;
//        this.list = list;
    }
    @Override
    public int getCount() {
        if (data.camlist == null) return 0;
        if (data.camlist.items == null) return 0;
        return data.camlist.items.length;
//        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return data.camlist.items[i];
//        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private class ViewHolder{
        TextView first;
        ImageView img;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder holder;
        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.cam_row,null);
            holder = new ViewHolder();
            holder.first = (TextView) convertView.findViewById(R.id.cam_text_first);
            holder.img = (ImageView) convertView.findViewById(R.id.cam_img);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.first.setText(data.camlist.items[i].name+"\n" + data.camlist.items[i].dist);

        if (data.camlist.items[i].status == 0) {
            Picasso.with(convertView.getContext())
                    .load(R.drawable.web_cam_logo_yellow)
                    .placeholder(R.drawable.web_cam_logo_yellow)
                    .into(holder.img);
        } else if (data.camlist.items[i].status == 1) {
            Picasso.with(convertView.getContext())
                    .load(R.drawable.web_cam_logo_red)
                    .placeholder(R.drawable.web_cam_logo_red)
                    .into(holder.img);
        } else if (data.camlist.items[i].status == 2) {
            Picasso.with(convertView.getContext())
                    .load(R.drawable.web_cam_logo_green)
                    .placeholder(R.drawable.web_cam_logo_green)
                    .into(holder.img);
        } else {
            Picasso.with(convertView.getContext())
                    .load(R.drawable.web_cam_logo)
                    .placeholder(R.drawable.web_cam_logo)
                    .into(holder.img);

        }

        return convertView;
    }
}
