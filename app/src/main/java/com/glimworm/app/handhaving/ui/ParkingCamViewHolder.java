package com.glimworm.app.handhaving.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.glimworm.app.handhaving.R;
import com.glimworm.app.handhaving.responses.parkingcams.ParkingCam;
import com.glimworm.app.handhaving.ui.listeners.OnItemClickListener;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by lars on 30-03-16.
 */
public class ParkingCamViewHolder extends SimpleViewHolder<ParkingCam> {

    private SimpleDateFormat formatter = new SimpleDateFormat("MM-dd HH:mm", Locale.getDefault());
    private Context context;
    private OnItemClickListener<ParkingCam> onClickListener;

    View view;

    @Bind(R.id.title)
    TextView title;

    @Bind(R.id.image)
    ImageView image;

    @Bind(R.id.date)
    TextView date;

    public ParkingCamViewHolder(
            @NonNull Context context,
            @NonNull View view,
            @Nullable OnItemClickListener<ParkingCam> onClickListener) {
        super(view);
        this.view = view;
        this.context = context;
        this.onClickListener = onClickListener;
        ButterKnife.bind(this, view);
    }

    @Override
    public void bind(final ParkingCam item) {
        title.setText(item.getKey());
        switch (item.getStatus()){
            case EMPTY:
                image.setImageDrawable(
                        context.getResources().getDrawable(R.drawable.web_cam_logo_2_0)
                );
                break;
            case CAR:
                image.setImageDrawable(
                        context.getResources().getDrawable(R.drawable.web_cam_logo_2_1)
                );
                break;
            case TRUCK:
                image.setImageDrawable(
                        context.getResources().getDrawable(R.drawable.web_cam_logo_2_2)
                );
            case CAR_VIOLATION:
                image.setImageDrawable(
                        context.getResources().getDrawable(R.drawable.web_cam_logo_2_3)
                );
                break;
            case TRUCK_VIOLATION:
                image.setImageDrawable(
                        context.getResources().getDrawable(R.drawable.web_cam_logo_2_4)
                );
                break;
        }
        date.setText(formatter.format(item.getTimestamp()));

        if(onClickListener != null){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(item, view, getAdapterPosition());
                }
            });
        }
    }
}