package com.glimworm.app.handhaving.helpers;

import com.glimworm.app.handhaving.MyCallbackInterface;
import com.glimworm.app.handhaving.responses.parkingcams.ParkingCam;
import com.glimworm.app.handhaving.services.LivingLabService;
import com.glimworm.app.handhaving.typeadapters.DateTypeAdapter;
import com.glimworm.app.handhaving.typeadapters.StatusTypeAdapter;
import com.glimworm.app.handhaving.xsd.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;
import java.util.Date;

import okhttp3.OkHttpClient;
import retrofit.converter.GsonConverter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jc on 19/11/15.
 */
public class API {

    private API(){}

    private static final OkHttpClient client = new OkHttpClient();

    private static LivingLabService livingLabService;
    private static Gson gson;

    public static LivingLabService getService(){
        if(livingLabService == null) {
            livingLabService = new Retrofit.Builder()
                    .baseUrl("http://dev.ibeaconlivinglab.com:1888/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build().create(LivingLabService.class);
        }
        return livingLabService;
    }

    public static Gson getGson(){
        if(gson == null){
            gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new DateTypeAdapter())
                    .registerTypeAdapter(ParkingCam.Status.class, new StatusTypeAdapter())
                    .create();
        }
        return gson;
    }
//
//    public static void getCams(MyCallbackInterface callback) {
//        String URL = "http://dev.ibeaconlivinglab.com:1880/parkingcams";
//        System.out.println("URL "+URL);
//        get(URL , camList.class, callback);
//    }
//
//
//
//    public static void ping(String FROMB, String LAT, String LON, MyCallbackInterface callback) {
//        String URL = "https://app.blushrr.com/wp-content/plugins/blushrr/admin/api.php?action=ping";
//        URL += "&lat="+((LAT != null) ? LAT : data.location.getLatitude());
//        URL += "&lon="+((LON != null) ? LON : data.location.getLongitude());
//        URL += "&id=" + data.un;
//        URL += "&ts="+new Date().getTime();
//        URL += "&bg=" + ((FROMB == null) ? "fg" : FROMB);
//        URL += "&bgc=0";   // config.bg_ping_cnt
//        URL += "&v=2.2";   // config,version
//        URL += "&lsa=na";
//        URL += "&lse=na";
//        URL += "&bgi=0";   // config,bg_ping_freq
//        URL += "&pusht=and";
//        URL += "&deviceid=devt";
////        post(URL, login.class, callback);
//    }

//    public static Call post(String URL, final Class CLASS,final MyCallbackInterface callback) {
//        return get("POST",URL,CLASS,callback);
//    }
//
//    public static Call get(String URL, final Class CLASS,final MyCallbackInterface callback) {
//        return get("GET",URL,CLASS,callback);
//    }
//    public static Call get(String TYPE, String URL, final Class CLASS,final MyCallbackInterface callback) {
//        Request request = new Request.Builder()
//                .url(URL)
//                .build();
//
//        if (TYPE.equalsIgnoreCase("POST")) {
//            FormEncodingBuilder feb = new FormEncodingBuilder();
//            String[] URLs = URL.split("[?]");
//            URL = URLs[0];
//            if (URLs.length > 1) {
//                String[] params = URLs[1].split("[&]");
//                for (int i=0; i < params.length; i++) {
//                    String param[] = params[i].split("[=]",2);
//                    if (param.length == 2) {
//                        feb.add(param[0],param[1]);
//                    }
//                }
//            }
//            RequestBody formBody = feb.build();
//            request = new Request.Builder()
//                    .url(URL)
//                    .post(formBody)
//                    .build();
//        }
//
//
//        Call c = client.newCall(request);
//        c.enqueue(new com.squareup.okhttp.Callback() {
//
//            @Override
//            public void onFailure(Request request, IOException throwable) {
//                throwable.printStackTrace();
//            }
//
//            @Override
//            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
//                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
//
//                com.squareup.okhttp.Headers responseHeaders = response.headers();
//                for (int i = 0; i < responseHeaders.size(); i++) {
//                    System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
//                }
//
//                String S = response.body().string();
//                System.out.println(S);
//
//                if (CLASS == camList.class) {
//
//                    GsonBuilder builder = new GsonBuilder();
//                    builder.registerTypeAdapter(Integer.class, new IntegerTypeAdapter());
//                    builder.registerTypeAdapter(Double.class, new DoubleTypeAdapter());
//                    Gson gson = builder.create();
//
//
//                    data.camlist = gson.fromJson(S, camList.class);
//                    System.out.println(data.camlist);
//                    System.out.println(gson.toJson(data.camlist));
//                    if (callback != null) callback.onDownloadFinished("OK");
//
//                }
//
//            }
//
//            @Override
//            protected Object clone() throws CloneNotSupportedException {
//                return super.clone();
//            }
//
//            @Override
//            public boolean equals(Object o) {
//                return super.equals(o);
//            }
//
//        });
//        return c;
//    }

}
