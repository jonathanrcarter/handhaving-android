package com.glimworm.app.handhaving.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by lars on 30-03-16.
 */
public abstract class SimpleViewHolder<T> extends RecyclerView.ViewHolder {

    public SimpleViewHolder(View view) {
        super(view);
    }

    public abstract void bind(T item);
}
