package com.glimworm.app.handhaving.ui.listeners;

import android.view.View;

/**
 * Created by lars on 05-04-16.
 */
public interface OnItemClickListener<T> {
    void onClick(T item, View view, int position);
}
