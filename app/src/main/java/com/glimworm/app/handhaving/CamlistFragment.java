package com.glimworm.app.handhaving;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.support.v4.app.Fragment;

import com.glimworm.app.handhaving.dummy.DummyContent;
import com.glimworm.app.handhaving.helpers.DoubleTypeAdapter;
import com.glimworm.app.handhaving.helpers.IntegerTypeAdapter;
import com.glimworm.app.handhaving.helpers.API;
import com.glimworm.app.handhaving.helpers.data;
import com.glimworm.app.handhaving.xsd.camListItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class CamlistFragment extends Fragment implements AbsListView.OnItemClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private CamViewAdapter mAdapter;
    private View mView;

    // TODO: Rename and change types of parameters
    public static CamlistFragment newInstance(String param1, String param2) {
        CamlistFragment fragment = new CamlistFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CamlistFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        // TODO: Change Adapter to display your content
//        mAdapter = new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
//                android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_camlist, container, false);

        // Set the adapter
        mListView = (AbsListView) mView.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);
        mAdapter = new CamViewAdapter(getActivity());
        mListView.setAdapter(mAdapter);
        mListView.setClickable(true);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                Object o = mListView.getItemAtPosition(position);
                camListItem item = (camListItem) o;
                data.currentCam = item;
//
//                data.currentChat.chan = item.channel;
//                data.currentChat.from = data.un;
//                data.currentChat.to = item.user.un;
//                data.currentChat.gr = item.user.gr;
//                data.currentChat.nn = item.user.nn;
//                data.currentChat.allowprofileclick = true;
//                data.currentChat.maxid = 0;
//
                Intent i = new Intent(getContext(), DetailActivity.class);
                startActivity(i);
            }
        });


        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Integer.class, new IntegerTypeAdapter());
        builder.registerTypeAdapter(Double.class, new DoubleTypeAdapter());
        Gson gson = builder.create();

//        String S = "{\"items\":[{\"id\":\"0\",\"name\":\"Fedrinandbol1\"},{\"id\":\"1\",\"name\":\"Fedrinandbol2\"}]}";
//
//        data.camlist = gson.fromJson(S, camList.class);
//        mAdapter.notifyDataSetChanged();

//
//
//        API.getCams(new MyCallbackInterface() {
//
//            @Override
//            public void onDownloadFinished(String result) {
//                Snackbar.make(mView, "Downloaded", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//                //don
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        mAdapter.notifyDataSetChanged();
//
//                    }
//                });
//            }
//        });

        return mView;
    }

    private void reload() {
//        API.getCams(new MyCallbackInterface() {
//
//            @Override
//            public void onDownloadFinished(String result) {
//                Snackbar.make(mView, "Downloaded", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//                //don
//                getActivity().runOnUiThreasd(new Runnable() {
//                getActivity().runOnUiThreasd(new Runnable() {
//                    @Override
//                    public void run() {
//                        mAdapter.notifyDataSetChanged();
//
//                    }
//                });
//            }
//        });

    }
    private boolean subscribed = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
            if (!subscribed) {
                subscribed = true;
                EventBus.getDefault().register(this);
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (subscribed) {
            subscribed = false;
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onStop() {
//        EventBus.getDefault().unregister(this);
//        super.onStop();
//    }
//    @Override
//    public void onPause() {
//        super.onPause();  // Always call the superclass method first
//    }


    @Subscribe
    public void onEvent(MessageEvent Event) {
        Log.d("app", "Event recieved!!");
        final MessageEvent event = Event;
        Log.d("app", "Event recieved!! " + event.action);
        if (event.action.equalsIgnoreCase("refresh-cam-list")) {
           try {
                mAdapter.notifyDataSetChanged();
                mListView.forceLayout();
            } catch (Exception E) {}
        }
        if (event.action.equalsIgnoreCase("refresh-cam-list-api")) {
            try {
                reload();
            } catch (Exception E) {}
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

}
