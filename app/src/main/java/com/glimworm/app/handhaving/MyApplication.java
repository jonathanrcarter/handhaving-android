package com.glimworm.app.handhaving;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by jc on 07/03/16.
 */
public class MyApplication extends Application implements BootstrapNotifier {

    private static final String TAG = ".MyApplicationName";
    private RegionBootstrap regionBootstrap;
    private ArrayList<Geofence> mGeofenceList = new ArrayList<Geofence>();
    private BackgroundPowerSaver backgroundPowerSaver;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "App started up");

        backgroundPowerSaver = new BackgroundPowerSaver(this);

        BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
        // To detect proprietary beacons, you must add a line like below corresponding to your beacon
        // type.  Do a web search for "setBeaconLayout" to get the proper expression.
        // beaconManager.getBeaconParsers().add(new BeaconParser().
        //        setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));

        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));

        // wake up the app when any beacon is seen (you can specify specific id filers in the parameters below)
        Identifier uuid = org.altbeacon.beacon.Identifier.parse("74278BDAB64445208F0C720EAF059935");
        Identifier major = org.altbeacon.beacon.Identifier.parse("1000");
        Region region = new Region("Glimworm Beacon", uuid, major, null);
//        regionBootstrap = new RegionBootstrap(this, region);

        String name = "fbol1";
        double latitude = 52.348973;
        double longitude = 4.891191;
        float radius = 100 * 20;


        long GEOFENCE_EXPIRATION_IN_MILLISECONDS = TimeUnit.DAYS.toMillis(365);

        mGeofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(name)

                .setCircularRegion(
                        latitude,
                        longitude,
                        radius
                )
                .setExpirationDuration(GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
//        if (mGeofencePendingIntent != null) {
//            return mGeofencePendingIntent;
//        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }
    @Override
    public void didDetermineStateForRegion(int arg0, Region arg1) {
        // Don't care
        Log.d(TAG, "Got B a did determine state for Region call "+ arg1.toString());
        Log.d(TAG, "Got B "+arg0 + " / "+arg1.getId1()+ " / "+arg1.getId2()+ " / "+arg1.getId3());
    }

    @Override
    public void didEnterRegion(Region arg0) {
        Log.d(TAG, "Got B a didEnterRegion call "+ arg0.toString());
        Log.d(TAG, "Got B / "+arg0.getId1()+ " / "+arg0.getId2()+ " / "+arg0.getId3());
        // This call to disable will make it so the activity below only gets launched the first time a beacon is seen (until the next time the app is launched)
        // if you want the Activity to launch every single time beacons come into view, remove this call.
//        regionBootstrap.disable();
        Intent intent = new Intent(this, MainActivity.class);
        // IMPORTANT: in the AndroidManifest.xml definition of this activity, you must set android:launchMode="singleInstance" or you will get two instances
        // created when a user launches the activity manually and it gets launched from here.
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    @Override
    public void didExitRegion(Region arg0) {
        // Don't care
        Log.d(TAG, "Got B a didExitRegion call "+ arg0.toString());
    }
}