package com.glimworm.app.handhaving.typeadapters;

import com.glimworm.app.handhaving.responses.parkingcams.ParkingCam;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by lars on 30-03-16.
 */
public class StatusTypeAdapter extends TypeAdapter<ParkingCam.Status> {

    @Override
    public void write(JsonWriter out, ParkingCam.Status value) throws IOException {
        if (value == null) {
            out.nullValue();
            return;
        }

        out.value(value.ordinal());
    }

    @Override
    public ParkingCam.Status read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }

        return ParkingCam.Status.values()[in.nextInt()];
    }
}
