package com.glimworm.app.handhaving;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.glimworm.app.handhaving.helpers.data;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DetailActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String LAT_KEY = "lat";
    public static final String LON_KEY = "lon";
    public static final String NAME_KEY = "name";

    private GoogleMap mMap;
    private double lat;
    private double lon;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        lat = getIntent().getDoubleExtra(LAT_KEY, 0.0);
        lon = getIntent().getDoubleExtra(LON_KEY, 0.0);
        name = getIntent().getStringExtra(NAME_KEY);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng position = new LatLng(lat, lon);
        mMap.addMarker(new MarkerOptions().position(position).title(name));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position,18.6f));
    }
}
