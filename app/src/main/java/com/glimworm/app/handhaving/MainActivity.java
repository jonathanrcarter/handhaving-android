package com.glimworm.app.handhaving;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.glimworm.app.handhaving.fragments.CamsFragment;
import com.glimworm.app.handhaving.helpers.data;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Collection;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BeaconConsumer,
        MapFragment.OnFragmentInteractionListener,
        CamlistFragment.OnFragmentInteractionListener {

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final String TAG = "main_activity";
    private BeaconManager beaconManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check 
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect beacons.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    //                    @eOvrride 
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }

                });
//                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                      
//
//                    //                    @eOvrride 
//                    public void onDismiss(DialogInterface dialog) {
//                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
//                    }
//                });  
                builder.show();
            }
        }
        beaconManager = BeaconManager.getInstanceForApplication(this);
        // To detect proprietary beacons, you must add a line like below corresponding to your beacon
        // type.  Do a web search for "setBeaconLayout" to get the proper expression.
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));

        beaconManager.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        CamsFragment _fragment = new CamsFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.relative_layout, _fragment)
                .commit();



    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            MessageEvent event = new MessageEvent();
//            event.action = "refresh-cam-list-api";
//            EventBus.getDefault().post(event);
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.nav_camara) {
            // Handle the camera action
            CamsFragment _fragment = new CamsFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.relative_layout, _fragment)
                    .commit();
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
        } else {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * eventbus
     */
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
    }

    @Subscribe
    public void onEvent(MessageEvent Event) {
        Log.d("app", "Event recieved!!");
        final MessageEvent event = Event;
        Log.d("app", "Event recieved!! " + event.action);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    @Override
    public void onFragmentInteraction(String S) {

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }

    private Identifier major1 = org.altbeacon.beacon.Identifier.parse("100");
    private Identifier major2 = org.altbeacon.beacon.Identifier.parse("200");
    private Identifier major3 = org.altbeacon.beacon.Identifier.parse("300");

    @Override
    public void onBeaconServiceConnect() {
        Log.i(TAG, "beacon Monitor");
        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                if (beacons.size() > 0) {
                    Beacon b = beacons.iterator().next();
                    Log.i(TAG, "Got MessageEventThe first beacon I see is about " + b.getDistance() + " meters away.");
                    if (data.camlist != null && data.camlist.items != null && data.camlist.items.length > 0) {
                        data.camlist.items[0].dist = b.getDistance();
                        if (b.getId1().equals(major1)) data.camlist.items[0].dist = 0;
                        if (b.getId1().equals(major2)) data.camlist.items[0].dist = 1;
                        if (b.getId1().equals(major3)) data.camlist.items[0].dist = 2;

                        MessageEvent event = new MessageEvent();
                        event.action = "refresh-cam-list";
                        EventBus.getDefault().post(event);

                    }
                }
            }
        });
        beaconManager.setMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                Log.i(TAG, "Got I just saw an beacon for the first time!");
                Log.d(TAG, "Got  " + region.getUniqueId() + " / " + region.getId1() + " / " + region.getId2() + " / " + region.getId3());
                Log.d(TAG, "Got  " + region.toString());

                try {
                    // start ranging for beacons.  This will provide an update once per second with the estimated
                    // distance to the beacon in the didRAngeBeaconsInRegion method.
                    beaconManager.startRangingBeaconsInRegion(region);
//                    beaconManager.setRangeNotifier(new RangeNotifier() {
//                        @Override
//                        public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
//                            if (beacons.size() > 0) {
//                                Log.i(TAG, "The first beacon I see is about " + beacons.iterator().next().getDistance() + " meters away.");
//                            }
//                        }
//                    });
                } catch (RemoteException e) {
                }
            }

            @Override
            public void didExitRegion(Region region) {
                Log.i(TAG, "Got I no longer see an beacon");
                Log.d(TAG, "Got  " + region.getUniqueId() + " / " + region.getId1() + " / " + region.getId2() + " / " + region.getId3());
                Log.d(TAG, "Got  " + region.toString());
                try {
                    beaconManager.startRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                }

            }

            @Override
            public void didDetermineStateForRegion(int state, Region region) {
                Log.i(TAG, "Got I have just switched from seeing/not seeing beacons: " + state);
                Log.d(TAG, "Got  " + region.getUniqueId() + "/ " + region.getId1() + " / " + region.getId2() + " / " + region.getId3());
                Log.d(TAG, "Got  " + region.toString());
            }
        });

        try {
            Identifier uuid = org.altbeacon.beacon.Identifier.parse("74278BDAB64445208F0C720EAF059935");
            Identifier major = org.altbeacon.beacon.Identifier.parse("1000");
            beaconManager.startMonitoringBeaconsInRegion(new Region("Glimworm Beacon", uuid, major, null));

            beaconManager.startMonitoringBeaconsInRegion(new Region("yellow", uuid, major1, null));

            beaconManager.startMonitoringBeaconsInRegion(new Region("red", uuid, major2, null));

            beaconManager.startMonitoringBeaconsInRegion(new Region("green", uuid, major3, null));

        } catch (RemoteException e) {    }
    }

}
