package com.glimworm.app.handhaving.responses;

import com.glimworm.app.handhaving.responses.parkingcams.ParkingCam;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lars on 10-03-16.
 */
public class ParkingCamsResponse{

    @SerializedName("items")
    private List<ParkingCam> cams;

    public List<ParkingCam> getCams() {
        return cams;
    }
}
