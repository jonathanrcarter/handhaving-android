package com.glimworm.app.handhaving;

/**
 * Created by jc on 01/11/15.
 */
public class MessageEventPayload {
    public String TEXT = "";
    public int INT = 0;
    public double DOUBLE = 0;
    public String url = "";
    public String imgurl = "";
    public String html = "";
    public String message = "";
}
